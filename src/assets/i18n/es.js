export const es = {
    'Welcome': 'Bienvenido a Juego de Tronos',
    'Hello': 'Hola',
    'Characters': 'Personajes',
    'Houses': 'Casas',
    'Chronology': 'Cronologia',
    'Search': 'Buscar',
    'Alliances' : 'Alianzas',
    'Appearances' : 'Apariciones',
    'Father' : 'Padre',
    'Offspring' : 'Descendecia',
    'Titles' : 'Titulos',
    'Motto' : 'Lema',
    'Location' : 'Sede',
    'Region' : 'Region',
    'Religions' : 'Religiones',
    'Fundation' : 'Fundacion'
}