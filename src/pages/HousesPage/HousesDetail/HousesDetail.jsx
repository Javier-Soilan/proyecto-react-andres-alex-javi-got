import axios from "axios";
import { useHistory, useParams } from "react-router-dom";
import { useEffect, useState,  } from "react";
import { Translator } from "../../../shared/Translator/Translator";
import go_Back from "../../../assets/FlagsIcons/go_back.svg";
import { BtnHome } from "../../../shared/BtnHome/BtnHome";
import Houses from "../../../assets/images/alt_houses.jpg";
import { useTranslation } from "react-i18next";
import './HousesDetail.scss';

export function HousesDetail() {

    const[house,setHouses]= useState([])

    const {name} = useParams();

    const getHouse = () =>{
        axios('https://api.got.show/api/show/houses/' + name).then(res=>{
        console.log(res.data)
        setHouses(res.data[0])
        });
    }

    useEffect(()=> getHouse(),[getHouse])

    const history = useHistory();

    const goBack = () =>{
        history.push('/houses');
    }

    const { t, i18n } = useTranslation(["translation"]);

    return (

        <div className="c-houses-detail">

            <BtnHome/>
            <Translator/>

            <div className="c-houses-detail__btn" onClick={goBack}>
                <img src={go_Back} className="c-houses-detail__arrow"/><span className="c-houses-detail__text">Volver</span>
            </div>

            <div className="c-houses-detail__div">

                <div className="c-houses-detail__photo">
                    {house.logoURL ? <img className="c-gallery__img" src={house.    logoURL} alt=''/> : <img className="c-gallery__img"  src={Houses}   atl="" />}
                    <h2>{house.name}</h2>
                </div>

            </div>

            <div className="c-houses-detail__info">

                <div>

                    <h3 className="title">{t("Motto")}</h3>

                    <div className="c-houses-detail__column">
                        <p>{house.words}</p>
                    </div>

                </div>

                <div>

                    <h3 className="title">{t("Location")}</h3>

                    <div className="c-houses-detail__column">
                        <p>{house.seat}</p>
                    </div>

                </div>

                <div>

                    <h3 className="title">{t("Region")}</h3>

                    <div className="c-houses-detail__column">
                        <p>{house.region}</p>
                    </div>

                </div>

                <div>

                    <h3 className="title">{t("Alliances")}</h3>

                    <div className="c-houses-detail__column">
                        {Array.isArray(house.allegiance) ? house.allegiance.map((item,i)=>
                        <p key={i}>{item}</p>) : <p>{house.allegiance}</p>}
                    </div>

                </div>

                <div>

                    <h3 className="title">{t("Religions")}</h3>

                    <div className="c-houses-detail__column">
                        {Array.isArray(house.religion) ? house.religion.map((item,i)=>
                        <p key={i}>{item}</p>) : <p>{house.religion}</p>}
                    </div>

                </div>

                <div>

                    <h3 className="title">{t("Fundation")}</h3>

                    <div className="c-houses-detail__column">
                        <p>{house.createdAt}</p>
                    </div>

                </div>
            </div>
        </div>
        );
    }