import { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import axios from "axios";
import { GalleryHouses } from "../../shared/GalleryHouses/GalleryHouses";
import { SearchForm } from "../../shared/SearchForm/SearchForm";
import { Translator } from "../../shared/Translator/Translator";
import { BtnHome } from "../../shared/BtnHome/BtnHome";
import { Nav } from "../../core/Nav/Nav";
import { Loader } from "../../shared/Loader/Loader";
import './HousesPage.scss'

let allHouses = [];

export function HousesPage() {
  const { t, i18n } = useTranslation(["translation"]);

  const [houses, setHouses] = useState([]);
  const [loader, setLoader] = useState(true);

  const getHouses = () => {
    axios("https://api.got.show/api/show/houses").then((res) => {
      allHouses = res.data;
      console.log(res.data);
      setHouses(res.data);
      setLoader(false);
    });
  };

  const filterHouses = (filterHousesName) => {
    const housesFiltered = allHouses.filter((houses) =>
      houses.name.toLowerCase().includes(filterHousesName.toLowerCase())
    );
    setHouses(housesFiltered);
  };

  useEffect(getHouses, []);

  return (
    <div className="c-houses">
      <SearchForm fnFilter={filterHouses} />

      <BtnHome/>

      <Translator/>

      <div className="c-houses__gallery">
      {loader ? <Loader /> :  <GalleryHouses housesList={houses} /> }
      </div>

      <div className="c-houses__nav">
        <Nav/>
      </div>

    </div>
  );
}
