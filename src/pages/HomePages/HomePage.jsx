import { useTranslation } from "react-i18next";
import { Nav } from "../../core/Nav/Nav";
import { BtnHome } from "../../shared/BtnHome/BtnHome";
import { Translator } from "../../shared/Translator/Translator";
import './HomePage.scss'

export function HomePage () {
    const { t, i18n } = useTranslation(["translation"]);
    return(
        <div className="c-home">    
            <Translator/>
            <div className="c-home__title">
                <h1>Game Of Thrones</h1>
            </div>
            <Nav/>
        </div>
    )
};