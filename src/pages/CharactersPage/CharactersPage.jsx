import { useEffect, useState } from "react";
import axios from "axios";
import { Gallery } from "../../shared/Gallery/Gallery";
import "./CharactersPage.scss";
import { SearchForm } from "../../shared/SearchForm/SearchForm";
import { Translator } from "../../shared/Translator/Translator";
import { BtnHome } from "../../shared/BtnHome/BtnHome";
import { Nav } from "../../core/Nav/Nav";
import { Loader } from "../../shared/Loader/Loader";



let allCharacters = [];

export function CharactersPage() {

  const [characters, setCharacters] = useState([]);
  const [loader, setLoader] = useState(true);

  const getCharacters = () => {
    axios("https://api.got.show/api/show/characters").then((res) => {
      allCharacters = res.data;
      console.log(res.data);
      setCharacters(res.data);
      setLoader(false);
    });
  };

  const filterCharacters = (filterCharactersName) => {
    const charactersFilteres = allCharacters.filter((characters) =>
      characters.name.toLowerCase().includes(filterCharactersName.toLowerCase())
    );

    setCharacters(charactersFilteres);
  };

  useEffect(getCharacters, []);

  return (
    <div className="c-characters">

      <SearchForm fnFilter={filterCharacters} />

      <BtnHome />

      <Translator/>
      <div className="c-characters__gallery">
        {loader ? <Loader /> : <Gallery charactersList={characters} /> }
      </div>

      <div className="c-characters__nav">
        <Nav/>
      </div>

    </div>
  );
}
