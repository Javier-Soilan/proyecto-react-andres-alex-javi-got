import axios from "axios";
import { useHistory, useParams } from "react-router-dom";
import { useEffect, useState } from "react";
import go_Back from "../../../assets/FlagsIcons/go_back.svg";
import { Translator } from "../../../shared/Translator/Translator";
import { BtnHome } from "../../../shared/BtnHome/BtnHome";
import { useTranslation } from "react-i18next";
import './CharactersDetail.scss';

export function CharactersDetail() {

  const [character, setCharacters] = useState([]);

  const [houses, setHouses] = useState([]);

  const { name } = useParams();

  

  const getCharacter = () => {
    axios("https://api.got.show/api/show/characters/" + name).then((res) => {
      setCharacters(res.data);
      
    });
  };

  const getHouses = () => {
    axios("https://api.got.show/api/show/houses/" + character.house).then(
      (res) => {
        if (res.data[0] != undefined) {
          setHouses(res.data[0]);
        }
      }
    );
  };

  useEffect(() => getCharacter(), []);
  useEffect(() => getHouses(), [character.house]);

  const history = useHistory();

  const goBack = () => {
    history.push("/characters");
  };

  const { t, i18n } = useTranslation(["translation"]);

  return (

    <div className="c-characters-detail">

          <BtnHome />
          <Translator/>

          <div className="c-characters-detail__btn" onClick={goBack}>
            <img src={go_Back} className="c-characters-detail__arrow"/><span className="c-characters-detail__text">Volver</span>
          </div>

    <div className="c-characters-detail__div">

          <div className="c-characters-detail__photo">
            <img className="c-characters-detail__img" src={character.image} />
            <h2>{character.name} </h2>
          </div>

    </div>

    <div className="c-characters-detail__info">

          <div>

              <h3 className="title">{character.house}</h3>

              <div className="c-characters-detail__column">
                  <img className="c-characters-detail__shield" src={houses.logoURL} />
              </div>

          </div>

          <div>

                <h3 className="title">{t("Alliances")}</h3>

            <div className="c-characters-detail__column">
                  {Array.isArray(character.allegiances) ? character.allegiances.map((item,i)=>
                    <p key={i}>{item}</p>) : <p>{character.allegiances}</p>}
            </div>

          </div>

          <div>
                <h3 className="title">{t("Appearances")}</h3>

            <div className="c-characters-detail__column">

                  <div>
                      {Array.isArray(character.appearances) ? character.appearances.map((item,i)=>
                        <p key={i}>{item}</p>) : <p>{character.appearances}</p>}
                  </div>

            </div>
          </div>

          <div>

            <h3 className="title">{t("Father")}</h3>

            <div className="c-characters-detail__column">
                <p>{character.father}</p>
            </div>

          </div>

          <div>

              <h3 className="title">{t("Offspring")}</h3>

              <div className="c-characters-detail__column">
                {Array.isArray(character.siblings) ? character.siblings.map((item,i)=>
                  <p key={i}>{item}</p>) : <p>{character.siblings}</p>}
              </div>

          </div>

          <div>
              <h3 className="title">{t("Titles")}</h3>

              <div className="c-characters-detail__column">
                  {Array.isArray(character.titles) ? character.titles.map((item,i)=>
                    <p  key={i}>{item}</p>) : <p>{character.titles}</p>}
              </div>
            
          </div>
        
      </div>
    
    </div>
  );
}
