import "./App.css";
import { BrowserRouter as Router } from "react-router-dom";
import { Routes } from "./core/Routes/Routes";

function App() {
  return (
    <Router>
    
        <Routes/>

      {/* <div className="div">
        <Nav />
      </div> */}
    </Router>
  );
}

export default App;
