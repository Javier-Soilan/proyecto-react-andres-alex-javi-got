import {NavLink} from "react-router-dom";
import { useTranslation } from "react-i18next";
import './Nav.scss'

export function Nav() {

    const { t, i18n } = useTranslation(["translation"]);

    return(
    <div className="c-menu">
        <nav className="c-menu__nav">
           {/*  <NavLink className="c-menu__link" activeClassName="c-menu__link--active" exact to="/">{t("Home")}</NavLink> */}
            <NavLink className="c-menu__link" activeClassName="c-menu__link--active" to="/characters">{t("Characters")}</NavLink>
            <NavLink className="c-menu__link" activeClassName="c-menu__link--active" to="/houses">{t("Houses")}</NavLink>
            <NavLink className="c-menu__link" activeClassName="c-menu__link--active" to="/chrono">{t("Chronology")}</NavLink>
        </nav>
    </div>
    )
}