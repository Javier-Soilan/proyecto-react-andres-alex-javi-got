import { Route, Switch } from "react-router";
import { ChronoPage } from "../../pages/ChronoPage/ChronoPage";
import { CharactersPage } from "../../pages/CharactersPage/CharactersPage";
import { HousesPage } from "../../pages/HousesPage/HousesPage";
import { HomePage } from "../../pages/HomePages/HomePage";
import { CharactersDetail } from "../../pages/CharactersPage/CharactersDetail/CharactersDetail";
import { HousesDetail } from "../../pages/HousesPage/HousesDetail/HousesDetail";

export function Routes() {
  return (
    <Switch>
      <Route path="/characters/:name">
        <CharactersDetail />
      </Route>
      <Route  path="/houses/:name">
        <HousesDetail />
      </Route>

      <Route path="/chrono">
        <ChronoPage />
      </Route>

      <Route path="/characters">
        <CharactersPage />
      </Route>

      <Route path="/houses">
        <HousesPage />
      </Route>

      <Route path="/">
        <HomePage />
      </Route>
    </Switch>
  );
}
