import { Link } from "react-router-dom";
import Houses from "../../assets/images/shield.png";
import './GalleryHouses.scss';

export function GalleryHouses(props) {

    return(
        <div className="c-gallery-houses">
            <div className="row">
            {props.housesList.map((house, i ) => 
                <div className="col-12 col-sm-8 col-md-6 col-lg-4 col-xl-2" key={i}>
                    <div className="c-gallery-houses__div">
                        <Link to={"/houses/" + house.name}>
                        {house.logoURL ? 
                        <img className="c-gallery-houses__img" src={house.logoURL} alt=''/> : <img className="c-gallery-houses__img"  src={Houses} atl="" />}
                        
                        <p className="c-gallery-houses__name">{house.name}</p></Link>
                    </div>
                </div>
            )}
            </div>
        </div>
    )
}