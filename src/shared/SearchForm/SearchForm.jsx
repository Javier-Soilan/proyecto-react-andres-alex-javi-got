import { useForm } from "react-hook-form";
import { useTranslation } from "react-i18next";
import './SearchForm.scss'

export function SearchForm(props){
    const {register,handleSubmit} = useForm();

    const filter = data => {
        props.fnFilter(data.name);
    }

    const { t, i18n } = useTranslation(["translation"]);

    return(
    <form className="b-search">
        <label className="b-search__label"><span className="b-icon icon-search"></span>
            <input className="b-search__input" type="text" name="name" placeholder={t("Search")+ "..."} ref={register} onInput={handleSubmit(filter)}/>
        </label>

    </form>
    )
}