import { NavLink } from "react-router-dom";
import Home from "../../assets/FlagsIcons/home_icon.svg";
import './BtnHome.scss'

export function BtnHome() {

  return (
    <div>

      <div className="b-home">
          <NavLink to="/">
            <img className="b-home__img" src={Home} alt="Home" />
          </NavLink>
      </div>
    </div>
  );
}
