import { Link } from 'react-router-dom';
import './Gallery.scss';
import Character from "../../assets/images/alt_houses.jpg";

export function Gallery(props) {

    return(
        
        <div className="c-gallery">
            <div className="row">
            {props.charactersList.map((character, i ) => 
                <div className="col-12 col-sm-8 col-md-6 col-lg-4 col-xl-2" key={i}>
                    <div className="c-gallery__div">
                        <Link className="c-gallery__link" to={"/characters/" + character.name}>
                            {character.image ?
                            <img className="c-gallery__img" src={character.image} alt='character'/> : <img className="c-gallery__img" src={Character} alt='character'/> 
                            }
                            <p className="c-gallery__name">{character.name}</p>

                        </Link>
                    </div>
                </div>
            )}
            </div>
        </div>
    )
}