import { useTranslation } from "react-i18next";
import Spain from "../../assets/FlagsIcons/spain_1.svg";
import Uk from "../../assets/FlagsIcons/united-kingdom_1.svg";
import './Translator.scss'

export function Translator() {
  const { t, i18n } = useTranslation(["translation"]);

  const changeLanguage = (code) => {
    i18n.changeLanguage(code);
  };

  return (
    <div className="c-translator">

   

      {/* <button className="boton" type="button" onClick={() => changeLanguage("es")}> */}

      <div className="c-translator__flag">
      
          <img className="c-translator__img" src={Spain} alt={t("translation:es")} onClick={() => changeLanguage("es")}/>
     
          <img className="c-translator__img" src={Uk} alt={t("translation:en")} onClick={() => changeLanguage("en")}/>

      </div>

      {/* </button> */}
    </div>
  );
}
